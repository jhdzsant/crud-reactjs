import React from "react";

// core components
import PanelHeader from "components/PanelHeader/PanelHeader.js";

function dashboard() {  

  return (
    <>
      <PanelHeader
        content={
          <div className="header text-center">
            <h2 className="title">Welcome</h2>            
          </div>
        }
      />
    </>
  );
}

export default dashboard;
