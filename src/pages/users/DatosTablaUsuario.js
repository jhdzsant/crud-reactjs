
import React, { Component } from "react";
import { Button } from "reactstrap";

import Notify from 'react-notification-alert';
import usuarios from "services/user";
import Mensaje from "../../services/Mensaje";

class DatosTablaUsuario extends Component {
    constructor(props) {
        super(props);
        // definimos las variables en el state de acuerdo a la estructura json del post
        this.state = {
            Id: 0,
            visibleAlert: true,
            success: false,
            modal: false,
            atributos: {
                "Id": 0,
                "Email": "",
                "Password": ""
            }
        }
    }

    delete(Id) {
        localStorage.removeItem("deleteId");
        localStorage.setItem("deleteId",Id);
        this.props.delete();
    }

    muestraModal(Id){
        localStorage.removeItem("CRUDId");
        localStorage.setItem("CRUDId",Id);
        this.props.mostrarModal();
    }

    render() {
        const { Id, Email} = this.props.data;
        return (
            <tr key={Id}>
                <td className="text-center">{Id}</td>
                <td className="text-center">{Email}</td>
                <td className="text-center">
                    <Button className="btn-icon" color="success" size="sm" onClick={this.muestraModal.bind(this,Id)} >
                        <i className="fa fa-edit"></i>
                    </Button>{` `}
                    <Button className="btn-icon" color="danger" size="sm" onClick={this.delete.bind(this,Id)} >
                        <i className="fa fa-times" />
                    </Button>
                </td>
                <td>
                    <Notify ref="notify" />
                </td>
            </tr>
        )
    }
}

export default DatosTablaUsuario;