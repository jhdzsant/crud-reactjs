import React, { Component } from "react";
import Notify from 'react-notification-alert';
import Mensaje from "../../services/Mensaje";
import { FormGroup, Label, Input, Button, Col, Row, Form, } from "reactstrap";
import usuarios from "services/user";

class AgregarUsuario extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Id: 0,
            Email: "",
            Password: "",
        }
    }

    setAtributos() {
        console.log(this.state);
        var atributos = {
            "Id": this.state.Id,
            "Email": this.state.Email,
            "Password": this.state.Password,
        }
        return atributos;
    }

    enviarDatos(atributos) {
        usuarios.post(atributos)
            .then((data) => {
                var options = Mensaje.mostrar(1, "El usuario se agrego con exito");
                //this.refs.notify.notificationAlert(options);
            })
            .catch((err) => {
                console.log("error en agregar usuario");
                var options = Mensaje.mostrar(2, "Obtuve un error al agregar al usuario :" + err);
                //this.refs.notify.notificationAlert(options);
            }
            )
        //sleep para mostrar el notify 
        //setTimeout(() => { this.props.showModal(); }, 3000)
    }

    componentDidMount() {
        if (this.props.pantalla === 2) {
            const flagId = localStorage.getItem('CRUDId');
            this.getDatosUsuario(flagId);
        }
    }

    actualizaDatos(atributos) {

        usuarios.put(atributos.Id, atributos)
            .then((data) => {
                var options = Mensaje.mostrar(1, "Los datos se actualizaron con exito");
            })
            .catch((err) => {
                console.log("error en agregar usuario");
                var options = Mensaje.mostrar(2, "Obtuve un error al actualizar los datos del usuario :" + err);
            }
            )
        //setTimeout(() => { this.props.showModal(); }, 3000)
    }
    getDatosUsuario(Id) {
        usuarios.get(Id)
            .then((r) => {
                console.log(r);
                this.setState({ Id: r.data[0].Id });
                this.setState({ Email: r.data[0].Email });
                this.setState({ Password: r.data[0].Password });
            }).catch((err) => {
                console.log("Obtuve un error al obtener los usuarios", err);
            })
    }

    render() {
        const pantalla = this.props.pantalla;
        const accion = () => {
            if (pantalla === 2) {
                //modo Update
                var datos = this.setAtributos();
                this.actualizaDatos(datos);//(Id);
            } else {
                //modo insert
                datos = this.setAtributos();
                this.enviarDatos(datos)
            }
        }

        return (
            <React.Fragment>
                <div className="content">
                    <Form onSubmit={accion}>
                        <Row form>
                            <Col md={4}>
                                <FormGroup>
                                    <Label for="Id">Id</Label>
                                    <Input type="number" name="Id" id="Id" onChange={event => this.setState({ Id: event.target.value })} value={this.state.Id} disabled />
                                </FormGroup>
                            </Col>
                            <Col md={4}>
                                <FormGroup>
                                    <Label for="Email">Email</Label>
                                    <Input type="text" name="Email" id="Email" onChange={event => this.setState({ Email: event.target.value })} value={this.state.Email} />
                                </FormGroup>
                            </Col>
                            <Col md={4}>
                                <FormGroup>
                                    <Label for="Password">Password</Label>
                                    <Input type="text" name="Password" id="Password" onChange={event => this.setState({ Password: event.target.value })} value={this.state.Password} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Button color="success" onClick={accion}>Save</Button>{` `}
                        <Button color="danger" onClick={this.props.showModal}>Cancel</Button>
                    </Form>
                    <Notify ref="notify" />
                </div>

            </React.Fragment>
        )
    }
}

export default AgregarUsuario;