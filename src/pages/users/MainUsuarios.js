import React, { Component } from "react";
// reactstrap components
import { Card, CardHeader, CardBody, CardTitle, Table, Button, Modal, ModalBody } from "reactstrap";
// core components
import PanelHeader from "components/PanelHeader/PanelHeader.js";
//import API component 
import usuarios from "../../services/user.js";
import DatosTablaUsuario from "./DatosTablaUsuario.js";
import EncabezadoTablaUsuario from "./EncaezadoTablaUsuario.js";
import AgregarUsuario from "./AgregarUsuario.js";

class MainUsuarios extends Component {

    constructor() {
        super();
        this.state = {
            Id: 0,
            data: [],
            modal: false,
            pantalla: 0,
            atributos: {
                "Id": 0,
                "Email": "",
                "Password": "",
            }
        }

    }

    showModal(pantalla) {
        if (pantalla === 2) {
            this.setState({ pantalla: 2 });
        } else {
            //this.setState({ atributos: {} });
            this.setState({ pantalla: 1 });
        }

        if (this.state.modal === true) {
            this.setState({ modal: false });
            this.getData();
        } else {
            this.setState({ modal: true });
        }
    }

    getDatosUsuario(Id) {
        usuarios.get(Id)
            .then((result) => {
                this.setState({ atributos: result.data }); // seteamos el State
            }).catch((err) => {
                console.log("Obtuve un error al obtener los usuarios", err);
            })
    }

    // Get all rows
    componentDidMount() {
        this.getData();
    }
    getData(){
        usuarios.all()
            .then((result) => {
                //console.log(result.data);
                this.setState({ data: result.data }); // seteamos el State
            }).catch((err) => {
                console.log("Obtuve un error al obtener los usuarios", err);
            })
    }

    async delete() {
        let Id = localStorage.getItem("deleteId");
        await usuarios.delete(Id)
            .then(() => {                
                this.getData();
                localStorage.removeItem("deleteId");
            })
            .catch((err) => {
                alert("Wrong trying to delete the user" );
            })
    }

    render() {

        if (this.state.data.length === 0) {
            var datos = (
                <tr>
                    <td className="text-center">No Data</td>
                </tr>
            );
        } else {

            const mostrarModal = () => { this.showModal(2); };
            const deleteUser = () => { this.delete(); };

            datos = (
                this.state.data.map(function (item, key) {
                    return (
                        <DatosTablaUsuario key={key} data={item} mostrarModal={mostrarModal} delete={deleteUser} />
                    )
                })
            );
        }

        return (
            <React.Fragment>
                <PanelHeader size="sm" />
                <div className="content">
                    <Card>
                        <CardHeader>
                            <CardTitle tag="h4">
                            </CardTitle>
                            <Button color="primary" size="lg" className="mb-3 btn-round" onClick={this.showModal.bind(this, 1)} >
                                Add User
                            </Button>
                        </CardHeader>

                        <CardBody>
                            <Table responsive>
                                <thead>
                                    <EncabezadoTablaUsuario />
                                </thead>
                                <tbody>
                                    {datos}
                                </tbody>
                            </Table>
                        </CardBody>
                    </Card>
                </div>
                <div>
                    <Modal isOpen={this.state.modal} size="lg">
                        <ModalBody>
                            <AgregarUsuario showModal={this.showModal.bind(this)} pantalla={this.state.pantalla} />
                        </ModalBody>
                    </Modal>
                </div>

            </React.Fragment>
        );
    }

}

export default MainUsuarios;