/*!

=========================================================
* Now UI Dashboard PRO React - v1.5.0
=========================================================

* Product Page: https://www.creative-tim.com/product/now-ui-dashboard-pro-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Dashboard from "./pages/dashboard.js";

import LoginPage from "views/Pages/LoginPage.js";
import RegisterPage from "views/Pages/RegisterPage.js";
import UserPage from "views/Pages/UserPage.js";

// Users
import Usuarios from "./pages/users/MainUsuarios.js";


let routes = [{//HOME
    path: "/dashboard",
    name: "HOME",
    icon: "now-ui-icons design_app",
    component: Dashboard,
    layout: "/admin",
},
{//signup
    path: "/register-page",
    name: "Register Page",
    short: "Register",
    mini: "RP",
    component: RegisterPage,
    layout: "/auth",
    invisible: true
  },
  {//Signin
    path: "/login-page",
    name: "Login Page",
    short: "Login",
    mini: "LP",
    component: LoginPage,
    layout: "/auth",
    invisible: true
  },
  {//User
      path: "/Users",
      name: "Users",
      icon: "now-ui-icons media-1_album",
      component: Usuarios,
      layout: "/admin",
  },
  {
    path: "/user-page",
    name: "User Profile",
    mini: "UP",
    component: UserPage,
    layout: "/admin",
    invisible: true
  },
  
];

export default routes;