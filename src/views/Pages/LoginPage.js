import React, { Component } from "react";

// reactstrap components
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
  Form,
  Container,
  Col,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Button,
  Alert,
} from "reactstrap";

// core components
import nowLogo from "assets/img/now-logo.png";
import bgImage from "assets/img/bg14.jpg";
import auth from "services/auth.js"
import { stringify } from "qs";


class LoginPage extends Component {
  constructor() {
    super();
    this.state = {
      password: "",
      email: "",
    }
  }
  Login() {
    //let history = useHistory();
    let data = {
      email: this.state.email,
      password: this.state.password,
    };
    auth.login(data).then(r => {
      
      if (r.status == 200) {
        const token = r.headers['token'];
        console.log(token)
        localStorage.removeItem("objUser");
        localStorage.removeItem("token");
        let user = JSON.stringify(r.data);
        localStorage.setItem("objUser",user);   
        localStorage.setItem("token",token);        
        this.props.history.push('/admin/dashboard')        
      } else {
        alert(r.data)
      }

    }).catch(e => {
      console.log(e);
      alert("Ups, Email or Password is wrong")
    })
  }
  render() {
    return (
      <div>
        <div className="content">
          <div className="login-page">
            <Container>
              <Col xs={12} md={8} lg={12} className="ml-auto mr-auto">
                <Form>
                  <Card className="card-login card-plain">
                    <CardHeader>
                      <div className="logo-container">
                        <img src={nowLogo} alt="now-logo" />
                      </div>
                    </CardHeader>
                    <CardBody>
                      <InputGroup
                        className={
                          "no-border form-control-lg"
                        }
                      >
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="now-ui-icons users_circle-08" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="text"
                          placeholder="Email"
                          onChange={event => this.setState({ email: event.target.value })}
                        />
                      </InputGroup>
                      <InputGroup
                        className={
                          "no-border form-control-lg"
                        }
                      >
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="now-ui-icons text_caps-small" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          placeholder="Password"
                          onChange={event => this.setState({ password: event.target.value })}
                        />
                      </InputGroup>
                    </CardBody>
                    <CardFooter>
                      <Button
                        block
                        color="primary"
                        size="lg"
                        href="#pablo"
                        className="mb-3 btn-round"
                        onClick={this.Login.bind(this)}
                      >
                        Get Started
                      </Button>
                      <div className="pull-left">
                        <h6>
                          <a href="/auth/register-page" className="link footer-link">
                            Create Account
                          </a>
                        </h6>
                      </div>
                    </CardFooter>
                  </Card>
                </Form>
              </Col>
            </Container>
          </div>
        </div>
        <div
          className="full-page-background"
          style={{ backgroundImage: "url(" + bgImage + ")" }}>
        </div>
      </div>)
  }

}

export default LoginPage;
