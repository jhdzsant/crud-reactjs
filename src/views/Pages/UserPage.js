/*!

=========================================================
* Now UI Dashboard PRO React - v1.5.0
=========================================================

* Product Page: https://www.creative-tim.com/product/now-ui-dashboard-pro-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  Row,
  Col,
  Form,
  Input,
  FormGroup,
  Alert,
} from "reactstrap";

// core components
import PanelHeader from "components/PanelHeader/PanelHeader.js";
import usuarios from "services/user";

class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Id: 0,
      Email: "",
      Password: "",
    }
  }

  setAtributos() {
    console.log(this.state);
    var atributos = {
      "Id": this.state.Id,
      "Email": this.state.Email,
      "Password": this.state.Password,
    }
    return atributos;
  }

  componentDidMount() {
    const objUSer = JSON.parse(localStorage.getItem('objUser'));
    let flagId = objUSer.Id;
    this.getDatosUsuario(flagId);
  }
  getDatosUsuario(Id) {
    console.log(Id);
    usuarios.get(Id)
      .then((r) => {
        console.log(r);
        this.setState({ Id: r.data[0].Id });
        this.setState({ Email: r.data[0].Email });
        this.setState({ Password: r.data[0].Password });
      }).catch((err) => {
        console.log("Obtuve un error al obtener los usuarios", err);
      })
  }
  delete() {
    const objUSer = JSON.parse(localStorage.getItem('objUser'));
    let Id = objUSer.Id;
    usuarios.delete(Id)
      .then(() => {
        this.props.history.push('/auth/login-page')
      })
      .catch((err) => {
        console.log(2, "Wrong trying to delete the user" + err);
        //this.refs.notify.notificationAlert(options);
      })
  }
  actualizaDatos() {
    var datos = this.setAtributos();
    usuarios.put(datos.Id, datos)
      .then((r) => {
        alert("Datos actualizados correctamente");
      })
      .catch((err) => {
        console.log("error en agregar usuario", err);
      }
      )
  }
  render() {
    return (
      <>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                  <h5 className="title">Edit Profile</h5>
                </CardHeader>
                <CardBody>
                  <Form>
                    <Row>
                      <Col className="pr-1" md="4">
                        <FormGroup>
                          <label>Id</label>
                          <Input type="number" name="Id" id="Id" onChange={event => this.setState({ Id: event.target.value })} value={this.state.Id} disabled />
                        </FormGroup>
                      </Col>
                      <Col className="px-1" md="4">
                        <FormGroup>
                          <label>Email</label>
                          <Input type="text" name="Email" id="Email" onChange={event => this.setState({ Email: event.target.value })} value={this.state.Email} />
                        </FormGroup>
                      </Col>
                      <Col className="pl-1" md="4">
                        <FormGroup>
                          <label>Password</label>
                          <Input type="text" name="Password" id="Password" onChange={event => this.setState({ Password: event.target.value })} value={this.state.Password} />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="12">
                        <Button color="primary" size="lg" className="mb-3 btn-round" onClick={this.actualizaDatos.bind(this)}>
                          Actualizar
                        </Button>{' '}
                        <Button color="danger" size="lg" className="mb-3 btn-round" onClick={this.delete.bind(this)}>
                          Delete
                        </Button>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default User;
