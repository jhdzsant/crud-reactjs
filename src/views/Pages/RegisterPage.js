
import React, { Component } from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  CardFooter,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  Button,
} from "reactstrap";

// core components
import bgImage from "assets/img/bg16.jpg";
import userserv from "services/user";


class RegisterPage extends Component {
  constructor() {
    super();
    this.state = {
      password: "",
      email: "",
    }
  }
  create() {
    //let history = useHistory();
    let data = {
      email: this.state.email,
      password: this.state.password,
    };
    userserv.post(data).then(r => {
      console.log(r);
      if (r.status == 200) {
        this.props.history.push('/admin/dashboard')
      } else {
        alert(r.data)
      }

    }).catch(e => {
      console.log(e);
      alert("Ups, Email or Password is wrong")
    })
  }

  render() {
    return (
      <di>
        <div className="content">
          <div className="register-page">
            <Container>
              <Row className="justify-content-center">
                <Col lg={5} md={8} xs={12}>
                  <div className="info-area info-horizontal mt-5">
                    <div className="icon icon-primary">
                      <i className="now-ui-icons media-2_sound-wave" />
                    </div>
                    <div className="description">
                      <h5 className="info-title">Marketing</h5>
                      <p className="description">
                        We've created the marketing campaign of the website. It
                        was a very interesting collaboration.
                      </p>
                    </div>
                  </div>
                  <div className="info-area info-horizontal">
                    <div className="icon icon-primary">
                      <i className="now-ui-icons media-1_button-pause" />
                    </div>
                    <div className="description">
                      <h5 className="info-title">Fully Coded in React 16</h5>
                      <p className="description">
                        We've developed the website with React 16, HTML5 and CSS3.
                        The client has access to the code using GitHub.
                      </p>
                    </div>
                  </div>
                  <div className="info-area info-horizontal">
                    <div className="icon icon-info">
                      <i className="now-ui-icons users_single-02" />
                    </div>
                    <div className="description">
                      <h5 className="info-title">Built Audience</h5>
                      <p className="description">
                        There is also a Fully Customizable CMS Admin Dashboard for
                        this product.
                      </p>
                    </div>
                  </div>
                </Col>
                <Col lg={4} md={8} xs={12}>
                  <Card className="card-signup">
                    <CardHeader className="text-center">
                      <CardTitle tag="h4">Register</CardTitle>
                      <div className="social btns-mr-5">
                        <Button className="btn-icon btn-round" color="twitter">
                          <i className="fab fa-twitter" />
                        </Button>
                        <Button className="btn-icon btn-round" color="dribbble">
                          <i className="fab fa-dribbble" />
                        </Button>
                        <Button className="btn-icon btn-round" color="facebook">
                          <i className="fab fa-facebook-f" />
                        </Button>
                        <h5 className="card-description">or be classical</h5>
                      </div>
                    </CardHeader>
                    <CardBody>
                      <Form>
                        <InputGroup
                          className="input-group-focus"
                        >
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="now-ui-icons ui-1_email-85" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            placeholder="Email..."
                            onChange={event => this.setState({ email: event.target.value })}
                          />
                        </InputGroup>
                        <InputGroup
                          className="input-group-focus"
                        >
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="now-ui-icons text_caps-small" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="password"
                            placeholder="Password..."
                            onChange={event => this.setState({ password: event.target.value })}
                          />
                        </InputGroup>
                      </Form>
                    </CardBody>
                    <CardFooter className="text-center">
                      <Button
                        color="primary"
                        size="lg"
                        className="btn-round"
                        href="#pablo"
                        onClick={this.create.bind(this)}
                      >
                        Get Started
                      </Button>
                    </CardFooter>
                  </Card>
                </Col>
              </Row>
            </Container>
          </div>
        </div>
        <div
          className="full-page-background"
          style={{ backgroundImage: "url(" + bgImage + ")" }}
        />
      </di>
    )
  }
}

export default RegisterPage;
