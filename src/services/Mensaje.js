class Mensaje {

    mostrar(tipoMensaje, mensaje) {
        var type;
        switch (tipoMensaje) {
            case 1:
                type = "success";
                break;
            case 2:
                type = "warning";
                break;
            case 3:
                type = "danger";
                break;
            case 4:
                type = "info";
                break;
            default:
                type = "warning";
        }
        var options = {
            place: "tr",
            message: mensaje,
            type: type, // tipo dle mensaje
            icon: "now-ui-icons ui-1_bell-53", // icono que se utiliza en el mensaje
            autoDismiss: 3, // tiempo que muestra el mensaje
        };
        return options;
    }
}

export default new Mensaje();