import http from "./http-common";
const token = localStorage.getItem('token');
http.defaults.headers.common['token'] = token;
class users {
    all() {
        return http.get("users");
    }

    post(data) {
        return http.post("users", data);
    }

    delete(id) {
        return http.delete(`users/${id}`);
    }

    get(id) {
        return http.get(`users/${id}`);
    }

    put(id, data) {
        return http.put(`users/${id}`, data);
    }
}


export default new users();