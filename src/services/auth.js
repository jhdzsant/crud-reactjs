import http from "./http-common";

class auth {
    login(data) {
        return http.post("auth/signin", data);
    }
}


export default new auth();